<?php

namespace appnic\inputsforblade;

use Illuminate\Support\Facades\Blade;

class Processor
{
    public static function processHTML($field, $parameters, $alias = null) {
        $fields = Processor::getFields();
        $aliases = Processor::getAliases();

        $fieldConfig = $fields[$field];
        $replacements = $fieldConfig['defaults'];

        $classes = config('inputs')['classes'];
        $class = '';
        if(array_key_exists($alias, $classes) && !empty($classes[$alias])) {
            $class = $classes[$alias];
        } else if(array_key_exists($field, $classes) && !empty($classes[$field])) {
            $class = $classes[$field];
        }
        $replacements['class'] = $class.' '.config('inputs')['baseclass'];

        if($alias != null && array_key_exists($alias, $aliases)) {
            $aliasValues = $aliases[$alias]['values'];

            foreach($aliasValues as $key=>$value) {
                $replacements[$key] = $value;
            }
        }

        foreach($fieldConfig['attributes'] as $key=>$attribute) {
            if(array_key_exists($key, $parameters)) {
                $replacements[$attribute] = $parameters[$key];
            }
        }

        $processedHtml = preg_replace_callback("|\{([a-zA-Z0-9_]+)\}|", function($matches) use ($replacements) {
            if(array_key_exists($matches[1], $replacements)) {
                return $replacements[$matches[1]];
            } else {
                return '';
            }
        }, $fieldConfig['html']);
        return $processedHtml;
    }

    public static function getFields() {
        $config = config('inputs');
        return array_merge($config['builtin_fields'], $config['fields']);
    }

    public static function getAliases() {
        $config = config('inputs');
        return array_merge($config['builtin_aliases'], $config['aliases']);
    }

    public static function registerFields() {
        $fields = Processor::getFields();
        foreach($fields as $key=>$field) {
            Blade::directive($key, function($expression) use ($key) {
                $expression = str_start($expression, '[');
                $expression = str_finish($expression, ']');
                return "<?php echo appnic\inputsforblade\Processor::processHTML('".$key."', ".$expression."); ?>";
            });
        }
    }

    public static function registerAliases() {
        $aliases = Processor::getAliases();
        foreach($aliases as $alias=>$props) {
            Blade::directive($alias, function($expression) use ($alias, $props) {
                $expression = str_start($expression, '[');
                $expression = str_finish($expression, ']');
                return "<?php echo appnic\inputsforblade\Processor::processHTML('".$props['key']."', ".$expression.",'".$alias."'); ?>";
            });
        }
    }
}